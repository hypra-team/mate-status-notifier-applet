/*
 * Copyright (C) 2016 Alberts Muktupāvels
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include <glib/gi18n-lib.h>

#include "sn-applet.h"
#include "sn-host-v0.h"
#include "sn-item.h"

struct _SnApplet
{
  MatePanelApplet   parent;

  GtkWidget *box;

  GSList    *hosts;
  GSList    *items;
};

G_DEFINE_TYPE (SnApplet, sn_applet, PANEL_TYPE_APPLET)

static gint
compare_items (gconstpointer a,
               gconstpointer b)
{
  SnItem *item1;
  SnItem *item2;
  SnItemCategory c1;
  SnItemCategory c2;
  const gchar *id1;
  const gchar *id2;

  item1 = (SnItem *) a;
  item2 = (SnItem *) b;

  c1 = sn_item_get_category (item1);
  c2 = sn_item_get_category (item2);

  if (c1 < c2)
    return -1;
  else if (c1 > c2)
    return 1;

  id1 = sn_item_get_id (item1);
  id2 = sn_item_get_id (item2);

  return g_strcmp0 (id1, id2);
}

static void
reorder_items (GtkWidget *widget,
               gpointer   user_data)
{
  SnApplet *sn;
  gint position;

  sn = SN_APPLET (user_data);

  position = g_slist_index (sn->items, widget);
  gtk_box_reorder_child (GTK_BOX (sn->box), widget, position);
}

static void
item_added_cb (SnHost   *host,
               SnItem   *item,
               SnApplet *sn)
{
  g_object_bind_property (sn->box, "orientation",
                          item, "orientation",
                          G_BINDING_SYNC_CREATE);

  sn->items = g_slist_prepend (sn->items, item);
  gtk_box_pack_start (GTK_BOX (sn->box), GTK_WIDGET (item), FALSE, FALSE, 0);

  sn->items = g_slist_sort (sn->items, compare_items);
  gtk_container_foreach (GTK_CONTAINER (sn->box), reorder_items, sn);
}

static void
item_removed_cb (SnHost   *host,
                 SnItem   *item,
                 SnApplet *sn)
{
  gtk_container_remove (GTK_CONTAINER (sn->box), GTK_WIDGET (item));
  sn->items = g_slist_remove (sn->items, item);
}

static void
sn_applet_constructed (GObject *object)
{
  SnApplet *sn;
  SnHost *host;

  G_OBJECT_CLASS (sn_applet_parent_class)->constructed (object);
  sn = SN_APPLET (object);

  host = sn_host_v0_new ();
  sn->hosts = g_slist_prepend (sn->hosts, host);

  g_object_bind_property (sn, "size", host, "icon-size",
                          G_BINDING_DEFAULT);

  g_signal_connect (host, "item-added", G_CALLBACK (item_added_cb), sn);
  g_signal_connect (host, "item-removed", G_CALLBACK (item_removed_cb), sn);

  gtk_widget_show (GTK_WIDGET (object));
}

static void
sn_applet_dispose (GObject *object)
{
  SnApplet *sn;

  sn = SN_APPLET (object);

  if (sn->hosts != NULL)
    {
      g_slist_free_full (sn->hosts, g_object_unref);
      sn->hosts = NULL;
    }

  g_clear_pointer (&sn->items, g_slist_free);

  G_OBJECT_CLASS (sn_applet_parent_class)->dispose (object);
}

/* mostly stolen from notification_area applet */
static GtkOrientation
get_gtk_orientation_from_applet_orient (MatePanelAppletOrient orient)
{
  switch (orient)
    {
    case MATE_PANEL_APPLET_ORIENT_LEFT:
    case MATE_PANEL_APPLET_ORIENT_RIGHT:
      return GTK_ORIENTATION_VERTICAL;
    case MATE_PANEL_APPLET_ORIENT_UP:
    case MATE_PANEL_APPLET_ORIENT_DOWN:
    default:
      return GTK_ORIENTATION_HORIZONTAL;
    }
}

static void
sn_applet_change_orient (MatePanelApplet       *applet,
                         MatePanelAppletOrient  orient)
{
  SnApplet *sn = SN_APPLET (applet);

  if (MATE_PANEL_APPLET_CLASS (sn_applet_parent_class)->change_orient)
    MATE_PANEL_APPLET_CLASS (sn_applet_parent_class)->change_orient (applet, orient);

  gtk_orientable_set_orientation (GTK_ORIENTABLE (sn->box),
                                  get_gtk_orientation_from_applet_orient (orient));
}

static gboolean
sn_applet_button_press_event (GtkWidget      *widget,
                              GdkEventButton *event)
{
  /* Prevent the panel from poping up the applet's popup on the the items,
   * which may also popup a menu which then conflicts.
   * This doesn't prevent the menu from poping up on the applet handle. */
  if (event->button == 3)
    return TRUE;

  return GTK_WIDGET_CLASS (sn_applet_parent_class)->button_press_event (widget, event);
}

static gboolean
sn_applet_focus (GtkWidget        *widget,
                 GtkDirectionType  direction)
{
  SnApplet *sn = SN_APPLET (widget);

  /* We let the box handle the focus movement because we behave more like a
   * container than a single applet.  But if focus didn't move, we let the
   * applet do its thing. */
  if (gtk_widget_child_focus (sn->box, direction))
    return TRUE;

  return GTK_WIDGET_CLASS (sn_applet_parent_class)->focus (widget, direction);
}

static void
sn_applet_class_init (SnAppletClass *sn_class)
{
  GObjectClass *object_class;
  GtkWidgetClass *widget_class;
  MatePanelAppletClass *applet_class;

  object_class = G_OBJECT_CLASS (sn_class);
  widget_class = GTK_WIDGET_CLASS (sn_class);
  applet_class = MATE_PANEL_APPLET_CLASS (sn_class);

  object_class->constructed = sn_applet_constructed;
  object_class->dispose = sn_applet_dispose;

  widget_class->button_press_event = sn_applet_button_press_event;
  widget_class->focus = sn_applet_focus;

  applet_class->change_orient = sn_applet_change_orient;
}

static void
sn_applet_init (SnApplet *sn)
{
  MatePanelApplet *applet;
  MatePanelAppletFlags flags;
  MatePanelAppletOrient orient;
  AtkObject *accessible;

  applet = MATE_PANEL_APPLET (sn);

  accessible = gtk_widget_get_accessible (GTK_WIDGET (applet));
  atk_object_set_name (accessible, _("Panel Status Notifier"));

  flags = MATE_PANEL_APPLET_EXPAND_MINOR | MATE_PANEL_APPLET_HAS_HANDLE;
  orient = mate_panel_applet_get_orient (applet);

  mate_panel_applet_set_flags (applet, flags);

  sn->box = gtk_box_new (get_gtk_orientation_from_applet_orient (orient), 0);
  gtk_container_add (GTK_CONTAINER (sn), sn->box);
  gtk_widget_show (sn->box);
}


static gboolean
factory_callback (MatePanelApplet *applet,
                  const gchar *iid G_GNUC_UNUSED,
                  gpointer data G_GNUC_UNUSED)
{
  gtk_widget_show (GTK_WIDGET (applet));
  
  return TRUE;
}

/* OMFG HACK */
#define main applet_main
static int main (int argc, char **argv);
MATE_PANEL_APPLET_OUT_PROCESS_FACTORY ("StatusNotifierAppletFactory",
                                       SN_TYPE_APPLET,
                                       "status-notifier-applet",
                                       factory_callback, NULL);
#undef main


/* Quite dirty hack to provide the org.kde.StatusNotifierWatcher service
 * ourselves, in case the session doesn't already */

#include <signal.h>
#include <glib-unix.h>
#include "libstatus-notifier-watcher/gf-status-notifier-watcher.h"

static gboolean
signal_handler (gpointer data G_GNUC_UNUSED)
{
  gtk_main_quit ();

  return FALSE;
}

int
main (int     argc,
      char  **argv)
{
  int retval;
  GfStatusNotifierWatcher *service;

#if ! GLIB_CHECK_VERSION (2, 36, 0)
  g_type_init ();
#endif

  g_unix_signal_add (SIGTERM, signal_handler, NULL);
  g_unix_signal_add (SIGINT, signal_handler, NULL);
  service = gf_status_notifier_watcher_new ();

  retval = applet_main (argc, argv);

  g_object_unref (service);

  return retval;
}
